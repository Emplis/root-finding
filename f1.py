from decimal import Decimal, DecimalException

global func_interval

func_interval = [-1.5, 1.5]

def func(x):
	y = (x**7 - Decimal('1.01317') * x**6 - Decimal('3.13386') * x**5 + Decimal('2.91478') * x**4 + Decimal('2.57288') * x**3 - Decimal('2.02908') * x**2 - Decimal('0.333377') * x**1 + Decimal('0.14974'))

	return Decimal(str(y))

def derivate(x):
	y = (7 * x**6 - Decimal('6.07902') * x**5 - Decimal('15.6693') * x**4 + Decimal('11.65912') * x**3 + Decimal('7.71864') * x**2 - Decimal('4.05816') * x - Decimal('0.333377'))

	return Decimal(str(y))