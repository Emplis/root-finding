from decimal import Decimal, DecimalException

global func_interval

func_interval = [-1, 2.5]

def func(x):
	y = (x**17 + Decimal('5.087915') - Decimal('12.1519967') * x**14 + Decimal('2.1708') * x**13 + Decimal('0.874'))

	return Decimal(str(y))

def derivate(x):
	y = (17 * x**16 - Decimal('170.1279538') * x**13 + Decimal('28.220399999999998') * x**12)

	return Decimal(str(y))