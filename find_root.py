#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
from decimal import Decimal, DecimalException, DivisionByZero, getcontext



try:
	# ------------------------------------------------------------------------
	# File (without .py) where the function, the derivate and the interval is
	# ------------------------------------------------------------------------

	from fx import *

	# ------------------------------------------------------------------------

	try:
		func_interval
		func
		derivate
	except NameError:
		exit('Your function file is not complete. Please make sure to have "func_interval", "func" and "derivate" correctly defined.')
except ImportError:
	exit('No file found in the current directory.')



getcontext().prec = 28 # Update precision of decimal for arithmetic



def main():
	roots = findRoot(globals()['func_interval'])

	if len(roots) == 0:
		print('f(x) don\'t have real roots.')
	elif len(roots) == 1:
		print('Real root of f(x):')
		print('f(\033[01;32m' + str(roots[0]) + '\033[00m) = ' + str(func(roots[0])))
	else:
		print('Real roots of f(x):')
		for i in range(0, len(roots)):
			print('f(\033[01;32m' + str(roots[i]) + '\033[00m) = ' + str(func(roots[i])))
		print('Number of roots: ' + str(len(roots)))

	exit(0)



# n est le nombre de découpe de l'intervalle
# n is the number of subdivision of the interval
#
# k est l'itérateur allant de 0 à n
# k is the iterator who goes from 0 to n

def findRoot(interval, n=1000): # default value for n is 100
	k = 0
	shiftedInterval = [0, 0]
	allResult = []

	try:
		h = (interval[1] - interval[0]) / n
	except DivisionByZero:
		exit('In findRoot: "n" need to be strictly greater than 0.')

	while k < n:
		shiftedInterval[0] = interval[0] + (k * h)
		shiftedInterval[1] = interval[0] + ((k + 1) * h)

		xStart = shiftedInterval[0]
		result = newtonRaphson(shiftedInterval, xStart)

		# Search in the interval with different xStart to ensure false negative

		if result == False:
			xStart = shiftedInterval[1]
			result = newtonRaphson(shiftedInterval, xStart)

			if result == False:
				xStart = random.uniform(shiftedInterval[0], shiftedInterval[1])
				result = newtonRaphson(shiftedInterval, xStart)

		# Real root found are are saved in the result array 

		if not result == False:
			allResult.append(result)

		k = k + 1

	return allResult



# N est le nombre maximum de boucle
#
# N is the maximum number of loop

def newtonRaphson(interval, xStart, N=Decimal('1000000000'), epsi=Decimal('0.000000000001')): # N = 1 billion , epsi = 1E-12
	x0 = Decimal(str(xStart))

	try:

		x1 = x0 - Decimal(str((func(x0) / derivate(x0))))

		while abs(x1 - x0) > epsi and N > 0:
			N = N - 1

			x0 = Decimal(str(x1))
			
			x1 = x0 - Decimal(str(func(x0) / derivate(x0)))

			if(x1 <= interval[0] or x1 >= interval[1]):
				return False

	except DivisionByZero:
		return False

	# If N number of loop has been made we assume their is no root in the interval

	if N == 0:
		return False
	else:
		return x1



main()