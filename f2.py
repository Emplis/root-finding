from decimal import Decimal, DecimalException

global func_interval

func_interval = [-3, 3]

def func(x):
	y = (x**4 - 4 * x**2 + x + 1)

	return Decimal(str(y))

def derivate(x):
	y = (4 * x**3 - 8 * x + 1)

	return Decimal(str(y))