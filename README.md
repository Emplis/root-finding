# Root finding

Find real roots of a given polynomial.

## How To

Import the project or at least `find_root.py`. 
Make sure to have python up to date, this project has been developed with python 3.5.2 and hasn't been tested on previous version.
To find roots of new function, you need to create a new python file (we recommend you to use the template file called `fx.py`, you just have to update some value) in the same directory as the `find_root.py` and then, implement the following things:

|Type of object|Name|Description|
|-------------:|:---|:----------|
| variable | `func_interval` | Interval of the function where you want to search the roots. It's an array `[a,b]` where `a` and `b` is respectivly, the minimum and, the maximum of the interval. They can be integer or floating number. **Important:** the `[a,b]` interval is a closed interval. |
| function | `func(Decimal(x))` | Implementation of your function. |
| function | `derivate(Decimal(x))` | Derivate of your function |

For the two function, if `y` is your result, return your y like that `Decimal(str(y))`.
Also, if you have floating number in your expression make sure to convert them in string (with `str(...)`) and convert the result in decimal (with `Decimal(...)`).

When your function file is ready, you can import it in the main file (`find_root.py`). To do that, replace the line 14 in the main file with the following instruction `from myfunction import *` just replace myfunction by the name of your file without the `.py`.

Now to run the program, start a terminal, move to the folder where `find_root.py` and your function file are. Give to `find_root.py` the permission to be executed (`chmod +x find_root.py`), then execute it (`./find_root.py`), wait a few second and if there is no problem you will now have all the real roots of your function in the given interval.

## Example

Example of a function file and the needed modification to `find_root.py`.
In this example my function file will be named `myFunction.py`.

The function will be: 

$$ f(x) = x^{17} + 5.087915 - 12.1519967 x^{14} + 2.1708 x^{13} + 0.874 $$

And is derivate:

$$ f'(x) = 17 x^{16} - 170.1279538 x^{13} + 28.220399999999998 x^{12} $$

We will search roots in `[-1, 2.5]`

`myFunction.py`:
```py
from decimal import Decimal, DecimalException

global func_interval

func_interval = [-1, 2.5]

def func(x):
	y = (x**17 + Decimal('5.087915') - Decimal('12.1519967') * x**14 + Decimal('2.1708') * x**13 + Decimal('0.874'))

	return Decimal(str(y))

def derivate(x):
	y = (17 * x**16 - Decimal('170.1279538') * x**13 + Decimal('28.220399999999998') * x**12)

	return Decimal(str(y))
```

`find_root.py`:
```py
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
from decimal import Decimal, DecimalException, DivisionByZero, getcontext



try:
	# ------------------------------------------------------------------------
	# File (without .py) where the function, the derivate and the interval is
	# ------------------------------------------------------------------------

	from myFunction import *

	# ------------------------------------------------------------------------

[...]
```

After executing the program you will normaly see that:

```plaintext
Real roots of f(x):
f(-0.9349361091351066518220746479) = -6E-28
f(0.9710051836283588847560196902) = 3E-27
f(2.236120788244323512082341649) = -3.6E-22
Number of roots: 3
```