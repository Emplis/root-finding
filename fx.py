import math
from decimal import Decimal, DecimalException, DivisionByZero

global func_interval

# ------------------------------------------------------------------------
# Replace "a" and "b" by the minimum and the maximum of your interval
# ------------------------------------------------------------------------

func_interval = [a, b]

# ------------------------------------------------------------------------

def func(x):
    try:

        # ------------------------------------------------------------------------
        # Replace "YOUR_FUNCTION" by the expression of your function
        # ------------------------------------------------------------------------

	    y = (YOUR_FUNCTION)

        # ------------------------------------------------------------------------

    except DivisionByZero:
        exit('func: DivisionByZero')
    else:
	    return Decimal(str(y))

def derivate(x):
    try:

        # ------------------------------------------------------------------------
        # Replace "YOUR_DERIVATE" by the expression of the derivate of your function
        # ------------------------------------------------------------------------

	    y = (YOUR_DERIVATE)

        # ------------------------------------------------------------------------

    except DivisionByZero:
        exit('derivate: DivisionByZero')
    else:
	    return Decimal(str(y))