from decimal import Decimal, DecimalException

global func_interval

func_interval = [-1.5, 1.5]

def func(x):
	y = (x**7 - Decimal('0.180361') * x**6 - Decimal('3.69268') * x**5 + Decimal('0.483033') * x**4 + Decimal('4.08794') * x**3 - Decimal('0.232496') * x**2 - Decimal('1.23845') * x**1 - Decimal('0.0888665'))

	return Decimal(str(y))

def derivate(x):
	y = (7 * x**6 - Decimal('1.082166') * x**5 - Decimal('18.4634') * x**4 + Decimal('1.932132') * x**3 + Decimal('12.26382') * x**2 - Decimal('0.464992') * x - Decimal('1.23845'))

	return Decimal(str(y))