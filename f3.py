from decimal import Decimal, DecimalException

global func_interval

func_interval = [-3, 3]

def func(x):
	y = (x**6 + 9 * x**5 - 3 * x + 1)

	return Decimal(str(y))

def derivate(x):
	y = (6 * x**5 + 45 * x**4 - 3)

	return Decimal(str(y))